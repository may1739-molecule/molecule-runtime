# Project Molecule
Project Molecule forms a backbone for communication between different devices and handles concerns such as networking, availability, and security.  It is intended to be used by 3rd party developers and enthusiasts to link smart devices and create a connected home.  This repository provides the tools to create, configure, and deploy 3rd party `Applications` and `Atoms` on the `System`.

### Table of Contents
1. System Overview
2. Setup & Requirements
3. Tools Overview
4. Manifest Files
5. Examples

# System Description
The `System` is comprised of several nodes called `Atoms`. `Atoms` manage all communication in the `System` and enforce permissions. `Atoms` also manage and run 3rd party `Applications`.  These `Applications` run in separate processes and can communicate by sending `Messages` through the `System` using a provided API.

## Messages
Communication uses a Request Response architecture meaning that a Sendor will receive a response `Message` for each request `Message` sent.  `Messages` contain routing and protocol data, a sized data payload, and an optional data stream.  The optional data stream can be used to send large files or other types of data.

### Actions & Payload
`Messages` implement User or `System` defined protocols called `Actions`.  The `Action` type is stored in each `Message` and allows an `Application` The type of an `Action` is part of the header in a `Message` stored as a String in the `Message` to reason about the data that will be included in the payload and data streams.  For example, when an `Application` receives a __"FILE"__ `ACTION`, the message payload of a request will be the following enum:
~~~ rust
pub enum FileAction {
    LIST(String),   //List all files in the directory name stored in the contained string
    GET(String),    //Get the file with the name stored in the contained string
}
~~~
The response payload will be dependent on the reqest. i.e:
* On a List request, the Response Message will return a Vector of Strings representing the file name in a given directory.
* On a Get request, the Response Message will contain the status of the request and a Stream containing the file data.

### Routing
`Message` routing information is only processed on a request.  This processing sets up a path for the data to return.  Once a request has been received, the response follows this path back to the requestor.  If an error occurs, the requesting `Application` is notified of the failure.  

An `Atom` routes a request by analyzing the following `Message` parameters:
1. The `Action` protocol
2. An `Application` identifier filter
3. An `Atom` identifier filter

The Identifier filter is one of the following:
~~~
Any  - Resolves to one identifier
All  - Resolves to all identifiers
One  - Explicit identifier
Some - Explicit group of identifiers
~~~
Here are several examples of how a message would be for the given parameters:

| Action | Atom | App | Description |
|---|---|---|---|
| T | ALL | ALL | Broadcasts message to all applications on all atoms which can receive `Action` __T__ |
| T | ANY | ONE(id) | Routes a `Message` to an unspecified instance of the `Application` |
| T | ALL | ONE(id) | Routes a `Message` to all instances of the `Application` |
| T | ANY | SOME(ids) | Routes a `Message` to an unspecified instance of each `Application` |

## Applications
`Applications` are 3rd Party Binaries that use our `Messaging` API and are managed by an `Atom`.  They have a manifest file which specifies:
* A globally unique identifier
* A unique human readable name
* Supported `Actions`

### Permissions
A developer must declare the `Actions` an `Application` can send, receive, or broadcast in order for the `Atom` to route `Messages` and enforce permissions.  If no `Actions` are specified, the `Application` will be effectively cut off from the `System`.

### Extensibility
Applications allow the system to be extensible and connect many different devices.  To connect a Smart Thermostat, for example, an `Application` could implement a __"THERMOSTAT"__ `Action` which wraps calls to a 3rd party API.

# Setup & Requirements
To use Project Molecule, you will need to setup a development computer and several host devices running Linux.  The development computer must be able to compile binaries for Linux. You will also need to have redis installed on all of the host devices.  For the purposes of this tutorial, we will assume the development computer is running Linux and that you have a basic understanding of the command line.  The development computer will be used to create the `System Files` to be deployed on the host devices (`System`).

## Development Computer
To generate the `System Files` you will need to have rust and the molecule-runtime binaries installed on the development computer.  The following steps guide you through compiling the molecule-runtime binaries for use in other tutorials.

### 1. Install Rust & Redis
[www.rust-lang.org](https://www.rust-lang.org/en-US/)  
[www.redis.io](https://redis.io/)

### 2. Install the Molecule-Runtime
1. Download the most recent release [here](https://gitlab.com/may1739-molecule/molecule-runtime).
2. Unzip the release.
3. compile the runtime.

##### Note: This overwrites the `./bin` folder.

~~~ bash
./release.sh # This Script compiles molecule-runtime and outputs it to the ./bin folder
~~~

# Tools Overview
This repository contains a command line utility called `generate` to create new `Application` projects, configure `Atoms`, and generate the necessary `System Files`.  It will create a `./dat` folder to store development and other files necessary to produce `System Files`.

~~~ bash
./generate.sh # This script runs the generator binary located in the ./bin folder
~~~

## Usage
| cmd | description |
|---|---|
| app | Create a project-molecule `Application` project |
| atom | Configure a new `Atom` by generating its `Atom Manifest`, `Device Manifest`, and its `Redis Manifest` |
| system | determine which `Atoms` will be included in a `System` |
| bundle | Create `System Files` to be deployed on host devices |

## Directory Structure
Applications are stored in two places. Projects are stored in the `dat/dev` folder while precompiled binaries are stored in `dat/sys/app`.  The bundle command will look for apps in both of these folders when compiling an `Atom's` `System Files`.  The `dat/boot` folder contains the `System Files` for each atom.  This includes: the application binaries specified in the `Atom Manifest`, the `Device Manifest` for other host devices running in the `System`, and other configuration files.

~~~ bash
bin/
    resources/                  # These are the default files used to generate applications and atoms
        .gitignore              #
        app.rs                  # hello world example
        log.yaml                # configures logging
        redis.conf              # configures redis-server [see documentation](http://download.redis.io/redis-stable/redis.conf)
        release.sh              # Script to compile App
        setup.sh                # Add cargo to path
        db                      # starts database on host
        run                     # starts atom on host
    generate                    # system generator
dat/
    boot/
        [atom]/
            app/
                [app]/          # mirrors app/release folder
                    data/       
                    .app 
                    app.bin     
                ...
            atm/
                [atom]          # Device Manifest
                ...
            .atm                # Atom Manifest
            .redis              # Redis Manifest
            .gitignore          # ~ from bin/resources/
            db                  # ~ from bin/resources/ 
            log.yaml            # ~ from bin/resources/ 
            redis.conf          # ~ from bin/resources/ 
            run                 # ~ from bin/resources/
    dev/
        molecule_app__[app]/    # Put App Projects Here
            release/
                data/           # Any app files/resources
                .app            # App Manifest
                app.bin         # Runtime compiled using release.sh
            src/                # App Source folder
                app.rs          # ~ from bin/resources/
            release.sh          # ~ from bin/resources/
            setup.sh            # ~ from bin/resources/
            ...
    sys/
        app/
            [app]/             # Put Precompiled Apps here
                data/
                .app            # App Manifest
                app.bin
            ...
        atm/
            [atom]/
                .atom           # Atom Manifest
                .device         # Device Manifest
                .redis          # Redis Manifest
            ...
        [system].sys            # Defines the atoms that are part of a system
~~~            

## Manifest Files
The generate utility can only be used to create new Manifest Files.  To make updates, you will need to modify the manifest files directly.  Make sure you do not make your changes in the boot directory as these files will be deleted and overwritten when you run the bundle command.  All Manifest files are in the JSON format and can be found.  A description of each manifest file can be found below as well as how the sys and dev files are mapped to the boot directory.

### Application Manifest

##### (dev/molecule_app_[app]/release/.app, sys/app/[app]/.app -> boot/[atom]/app/[app/.app])
~~~ js
{
  "id": "",             // Uuid
  "name": "",           // Application Name
  "permissions": {
    "send": [""],       // Actions this Application can Send
    "receive": [""],    // Actions this Application can Receive
    "broadcast": [""]   // Actions this Application can Broadcast
  }
}
~~~

### Atom Manifest

##### (sys/atm/[atom]/.atom -> boot/[atom]/.atom)
~~~ js
{
  "atom": {
    "name": "",         // Atom Name
    "network_id": ""    // Internal Uuid
  },
  "device": {},         // Device Manifest
  "applications": [""], // Applications which will run on this system
  "system": "system"    // Name of the System which this Atom is a part of
}
~~~

### Device Manifest

##### (sys/atm/[atom]/.device -> boot/[atom]/atm/[other_atom])
~~~ js
{
    "id": "",           // Atom Id
    "address": "",      // IP Address:Port
    "sign_key": {},     // 128 bit signing key
    "cipher_key": {},   // 128 bit cryptographic key
}
~~~

### Redis Manifest

##### (sys/atm/[atom]/.redis -> boot/[atom]/.redis)
~~~ js
{
  "config": "",         // Relative or Absolute path to redis.conf file
  "socket": "",         // Absolute path to redis socket as specified in redis.conf
  "db": 0               // Redis db number
}
~~~

### System

##### (sys/[system].sys)
~~~ js
[""]                    // Names of Atoms in a System
~~~

# Examples
1. Hello World
2. Client Server
The following examples assume you are running linux and have the required tools and dependencies installed.  In this case we will be using our development machine as a host device.  In a real system, the boot files would be coppied to the host devices and then run.

## Hello World
To start out we will create the default Hello World `Application`.  This creates a server which watches for ___HELLO___ `Actions` and responds with an arbitrary `Message`.  It also sends out a ___HELLO___ `Action` when it starts up so we can see an output.  First open a terminal and follow the steps below:

### Create an Application
1. Run the generate script and select the `app` command.
2. Give it the name ___hello___.
3. When it asks for `Action` permissions add ___HELLO___ to the send, receive, and broadcast action lists.
4. When it finishes a new folder in the dev directory (`dev/molecule_app__hello`) will contain a default Hello World application.

##### Note: that Actions is case sensitive

### Configure an Atom
1. Run the generate script and select the `atom` command.
3. You can use the defaults except for the redis socket. You will need to specify the following absolute path (/path/to/dat/boot/atom/redis.conf).  At this point /atom/redis.conf will not exist as it is generated with the bundle command.
2. When you get to the apps section add the ___hello___ application to the list.

### Configure a System
1. Run the generate script and select the `system` command.
2. Give it the default name and add ___atom`___ to the list of atoms in the system.

### Bundle
1. Run the generate script and select the `bundle` command.
2. Bundle the system named system

#### _Note: bundling a system prepares a boot directory for all of its atoms.  If you would like to only bundle a particular atom, you can also specify just the atom's name._

### Code
Before we run the example lets take a look at the code.  It is located at `dev/molecule_app__hello/src/app.rs` This example uses the `futures` and the `tokio_core` crates to handle asynchronous communication.  We also use several modules from molecule_common to communicate with the host `Atom`.  Namely we use the ___Messaging___, ___Reqes___ ( __Req__ uest / R ___es___ ponse), ___Error___, and ___Application___ modules.  The ___Messaging___ Module provides utilities to serialize and deserialize message data and create responses.  The ___Reqes___ module is used to send and receive messages from the host `Atom`.  The ___Error___ Module defines the a custom error and result type.  Finally, the ___Application___ Module sets up communication with host `Atom`.

~~~ rust
extern crate tokio_core;
extern crate molecule_common;
extern crate uuid;
extern crate futures;

use futures::{BoxFuture, Future, Stream, future};
use molecule_common::app;
use molecule_common::err::*;
use molecule_common::message::*;
use molecule_common::reqes::*;
use tokio_core::reactor::Core;
use uuid::Uuid;
~~~

The following code segment starts an event loop core and sets up communication with the host `Atom`.

~~~ rust
fn main() {
    let mut core = Core::new().unwrap();
    let reqes: Reqes<Message> = app::start(core.handle()).unwrap();

    ...
~~~

Here we setup a Server to process incoming `Messages` and generate responses.  We are only looking for ___HELLO___ actions and expect the payload to be a string.  After setting everything up, we spawn it in the event loop.
~~~ rust
    ...

    let handle = core.handle();

    //Listen For Messages
    let in_server = reqes
        .server
        .receive()
        .and_then(move |mut req| -> BoxFuture<(), Error> {
            //Get Message
            let msg: Message = req.take_data().unwrap();
            //Determine the Action
            let action: String = msg.get_action().into();
            //Process the Actions
            let result: Box<Future<Item = Message, Error = Error>> = match action.as_ref() {
                "HELLO" => {
                    //Get Message Data
                    let data: Result<String> = msg.get_data();
                    let data: Result<&str> = match data {
                        Ok(ref s) => Ok(s.as_ref()),
                        Err(e) => Err(e),
                    };
                    //Determine Response
                    match data {
                        Ok("hello") => {
                            let response = msg.response()
                                .data(Ok("hello to you too!"))
                                .trace("HELLO_WORLD", Uuid::nil())
                                .build_packet();
                            Box::new(future::ok(response))
                        }
                        _ => {
                            let response = msg.response()
                                .data::<()>(Err(Error::new(ErrKind::Other,
                                                           "I only understand one word")))
                                .trace("HELLO_WORLD", Uuid::nil())
                                .build_packet();
                            Box::new(future::ok(response))
                        }
                    }
                }
                // If you support more actions, implement them here
                _ => unimplemented!(),
            };
            //Send Response
            req.reply(&handle, result);
            future::ok(()).boxed()
        })
        .map_err(|_| ())
        .for_each(|_| -> BoxFuture<(), ()> { future::ok(()).boxed() });

    // Run Server in Event Loop
    core.handle().spawn(in_server);

    ...
~~~

Here we setup a Client to send a ___HELLO___ `Message` when the `Application` starts. It then waits for a response from the server.  In a future example, we will run the client and server in seperate applications.  After setting everything up, we spawn it in the event loop.
~~~ rust
    ...

    //Send a Message Somewhere
    let request = reqes
        .client
        .send(MessageBuilder::new("HELLO")
                  .source(Signature::broadcast())
                  .destination(Signature::broadcast())
                  .data("hello")
                  .unwrap()
                  .trace("HELLO_WORLD", Uuid::nil())
                  .build_packet())
        .map(|msg: Message| {
                 println!("I received a message {:?}", msg);
             })
        .map_err(|_| ());

    // Run Client in Event Loop
    core.handle().spawn(request);

    ...
~~~

If we wanted to setup another future, we could do so here before we start the event loop.  Once everything is set up we start the event loop which blocks the thread.  For more information on tokio and futures see the documentation [here](https://tokio.rs/).
~~~ rust
    ...

    // Start Event Loop
    loop {
        core.turn(None);
    }
}
~~~

### Deploy
1. In a terminal, change directories into the dat/boot/atom folder
2. run `./db`
3. In a second terminal, also change directories into the dat/boot/atom folder
4. run `./run`

If everything went well, The `hello` app should output something like the following:

~~~ bash
I received a message Message {
        id: f02d55b3-4253-4620-9784-052d7c38962d
        routing: None
        msg_type: Packet
        action: HELLO
        data: Ok([{"Ok":{"id":"f02d55b3-4253-4620-9784-052d7c38962d","routing":null,"msg_type":"Packet","action":"HELLO","data":{"Ok":"\"hello to you too!\""},"trace":{"trace":[["HELLO_WORLD","00000000-0000-0000-0000-000000000000"],["Atom::run()","22945895-7a17-4266-8dbc-3af16503066b"],["Atom::incoming","22945895-7a17-4266-8dbc-3af16503066b"],["HELLO_WORLD","00000000-0000-0000-0000-000000000000"]]}}}])
        trace: [ (HELLO_WORLD   00000000-0000-0000-0000-000000000000) (Atom::run() 22945895-7a17-4266-8dbc-3af16503066b) (   00000000-0000-0000-0000-000000000000) ]
}
~~~

## Hello World Client Server Example
To make this example more interesting let's create a new `System` with two applications named `hello_client` and `hello_server`.
### Create the Applications
Follow the same steps as before but use `hello_client` and `hello_server` as the application names.  Once you have made both apps navigate to the source code.  In `hello_client`, remove the Serve code.  In `hello_server`, remove the client code.  If you need clarification on what code to delete, review the previous section describing how the code works.

### Configure an Atom
Next create two new atoms named `atom_client` and `atom_server`.  Follow the same steps as before with the following modifications:
1. You will need to choose two different ports for the network socket.
2. The absolute path to the redis.conf file will be (`/path/to/boot/[atom_name]/redis.conf`).
3. In the apps section add `hello_client` to `atom_client` and `hello_server` to `atom_server`.
4. Set the `System` name to  `two_atoms`

### Configure a System
Next rerun the system command.  Name the system `tow_atoms` and add both `atom_client` and `atom_server`

### Bundle
Finally Run the bundle command on the `two_atoms` `System`. Your two atoms are ready to deploy.

### Deploy
To Run the atom's, we will need to open several terminals:
1. In the first terminal change directory into dat/boot/atom_server/ and run the `./db` binary.
2. In another terminal go to the same directory and run the `./run` binaries.
3. Do the same for the atom_client

##### Note: if you run the client before the server, the example will not work.  The client does not wait for the server to be running in the current implementation.

If everything went correctly, you should see the same message as before in the Atom Client.

### Recap & Further Exploration
This example demonstrates how a message can be broadcast and how apps on different devices can respond to a message. For further exploration, try adding a third `Atom` running the `hello_server` app.  The `hello_client` should see two responses to its hello world message.