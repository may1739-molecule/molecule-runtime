#[macro_use]
extern crate molecule_common;

use molecule_common::manifest::*;
use molecule_common::path::*;
use molecule_common::state::redis::*;

fn main() {
    let config: RedisManifest = open_file(&prefix___here!(atm___redis!())).unwrap();
    RedisServer::new(&config)
        .child
        .wait_with_output()
        .unwrap();
}