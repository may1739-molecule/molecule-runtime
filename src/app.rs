extern crate tokio_core;
extern crate molecule_common;
extern crate uuid;
extern crate futures;

use futures::{BoxFuture, Future, Stream, future};
use molecule_common::app;
use molecule_common::err::*;
use molecule_common::message::*;
use molecule_common::reqes::*;
use tokio_core::reactor::Core;
use uuid::Uuid;

fn main() {
    let mut core = Core::new().unwrap();
    let reqes: Reqes<Message> = app::start(core.handle()).unwrap();

    let handle = core.handle();

    //Listen For Messages
    let in_server = reqes
        .server
        .receive()
        .and_then(move |mut req| -> BoxFuture<(), Error> {
            //Get Message
            let msg: Message = req.take_data().unwrap();
            //Determine the Action
            let action: String = msg.get_action().into();
            //Process the Actions
            let result: Box<Future<Item = Message, Error = Error>> = match action.as_ref() {
                "HELLO" => {
                    //Get Message Data
                    let data: Result<String> = msg.get_data();
                    let data: Result<&str> = match data {
                        Ok(ref s) => Ok(s.as_ref()),
                        Err(e) => Err(e),
                    };
                    //Determine Response
                    match data {
                        Ok("hello") => {
                            let response = msg.response()
                                .data(Ok("hello to you too!"))
                                .trace("HELLO_WORLD", Uuid::nil())
                                .build_packet();
                            Box::new(future::ok(response))
                        }
                        _ => {
                            let response = msg.response()
                                .data::<()>(Err(Error::new(ErrKind::Other,
                                                           "I only understand one word")))
                                .trace("HELLO_WORLD", Uuid::nil())
                                .build_packet();
                            Box::new(future::ok(response))
                        }
                    }
                }
                // If you support more actions, implement them here
                _ => unimplemented!(),
            };
            //Send Response
            req.reply(&handle, result);
            future::ok(()).boxed()
        })
        .map_err(|_| ())
        .for_each(|_| -> BoxFuture<(), ()> { future::ok(()).boxed() });

    core.handle().spawn(in_server);

    //Send a Message Somewhere
    let request = reqes
        .client
        .send(MessageBuilder::new("HELLO")
                  .source(Signature::broadcast())
                  .destination(Signature::broadcast())
                  .data("hello")
                  .unwrap()
                  .trace("HELLO_WORLD", Uuid::nil())
                  .build_packet())
        .map(|msg: Message| {
                 println!("I received a message {:?}", msg);
             })
        .map_err(|_| ());

    core.handle().spawn(request);

    loop {
        core.turn(None);
    }
}