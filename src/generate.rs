extern crate serde_json;
extern crate serde;
#[macro_use]
extern crate molecule_common;
extern crate uuid;

use molecule_common::manifest::*;
use molecule_common::path::*;
use std::fmt;
use std::fs;
use std::io::Write;
use std::net::SocketAddr;
use std::process::Command;
use uuid::Uuid;

fn main() {
    let pad: &str = "";
    let pad2: &str = &next_pad(pad);

    create_dir(fldr___dat!()).is_ok();
    create_dir(fldr___sys!()).is_ok();
    create_dir(&prefix___sys!(atm___fldr!())).is_ok();
    create_dir(&prefix___sys!(app___fldr!())).is_ok();
    create_dir(fldr___dev!()).is_ok();
    create_dir(fldr___boot!()).is_ok();

    loop {
        println!("Manifest Generator: (bundle, system, atom, app, exit)");
        let cmd: &str = &read_line("");
        match cmd {
            "bundle" => bundle(pad2),
            "system" => system(pad2),
            "atom" => atom(pad2),
            "app" => app(pad2),
            "usage" => usage(pad2),
            "exit" => return,
            _ => usage(pad2),
        }
    }
}

fn read_line(pad: &str) -> String {
    print!("{}", pad);
    std::io::stdout().flush().unwrap();
    let mut input = String::new();
    std::io::stdin().read_line(&mut input).unwrap();
    input.trim().into()
}

fn bundle(pad: &str) {
    let pad2: &str = &next_pad(pad);

    println!("{}-- bundle atom or system?: (atom, system)", pad);
    let cmd: &str = &string_item(pad2, "atom").unwrap();
    match cmd {
        "atom" | "system" => (),
        _ => {
            println!("{} !invalid command", pad);
            return;
        }
    }

    println!("{}-- name", pad);
    let name: &str = &string_item(pad2, "").unwrap();

    match cmd {
        "atom" => bundle_helper(pad2, name),
        "system" => {
            match open_file::<Vec<String>>(&prefix___sys!(tmpl___sys!(name))) {
                Ok(atoms) => {
                    for atom in atoms {
                        bundle_helper(pad2, &atom);
                    }
                },
                Err(_) => println!("{} !invalid system file", pad2)
            }
        }
        _ => println!("{} !Invalid Entry", pad),
    }
}

fn bundle_helper(pad: &str, name: &str) {
    let atom_path = prefix___sys_atm!(name, atm___manifest!());
    let atom = open_file::<Manifest>(&atom_path);
    if atom.is_err() {
        println!("{} ! atom not found: {}", pad, atom_path);
        return;
    }
    let atom = atom.unwrap();
    println!("BUNDLE {}", atom.atom.name);

    //Create Boot Directory Structure
    create_dir(&prefix___boot!(name)).is_ok();
    create_dir(&prefix___boot_atm!(name, app___fldr!())).is_ok();
    create_dir(&prefix___boot_atm!(name, atm___fldr!())).is_ok();

    //Copy Log Config and Runtimes
    copy_file(&prefix___resources!(atm___log!()),
              &prefix___boot_atm!(name, atm___log!()))
            .unwrap();
    copy_file(&prefix___resources!("redis.conf"),
              &prefix___boot_atm!(name, "redis.conf"))
            .unwrap();
    copy_file(&prefix___here!("run"), &prefix___boot_atm!(name, "run")).unwrap();
    copy_file(&prefix___here!("db"), &prefix___boot_atm!(name, "db")).unwrap();

    //Copy Atom info
    copy_file(&prefix___sys_atm!(name, atm___manifest!()),
              &prefix___boot_atm!(name, atm___manifest!()))
            .unwrap();
    copy_file(&prefix___sys_atm!(name, atm___redis!()),
              &prefix___boot_atm!(name, atm___redis!()))
            .unwrap();
    //Copy App Binaries
    for app in atom.particles {
        let dev_path = prefix___dev_app!(app, dev_app___manifest!());
        let app_path = prefix___sys_app!(app, app___manifest!());
        let boot_path = prefix___boot_atm!(name, tmpl___app!(app));
        if fs::metadata(&dev_path).is_ok() {
            println!("BUNDLE.APP {}", app);
            Command::new(prefix___here!(dev_app___compile!()))
                .current_dir(prefix___dev!(tmpl___dev_app!(app)))
                .spawn()
                .unwrap()
                .wait_with_output()
                .unwrap();
            create_dir(&boot_path).is_ok();
            copy_fldr(&prefix___dev_app!(app, dev_app___release!()), &boot_path).is_ok();
        } else if fs::metadata(&app_path).is_ok() {
            create_dir(&boot_path).is_ok();
            copy_fldr(&prefix___sys!(tmpl___app!(app)), &boot_path).is_ok();
        } else {
            println!("{} ! app not found: {}", pad, app)
        }
    }

    //Copy Device Keys
    let system = prefix___sys!(tmpl___sys!(atom.system));
    let devices = open_file::<Vec<String>>(&system);
    if devices.is_err() {
        println!("{} ! system not found: {}", pad, system);
        return;
    }

    for dev in devices.unwrap() {
        let dev_path = prefix___sys_atm!(dev, atm___device!());
        if fs::metadata(&dev_path).is_ok() {
            copy_file(&dev_path, &prefix___boot_atm!(name, tmpl___atom!(dev))).unwrap();
        }
    }

}


fn system(pad: &str) {
    let pad2: &str = &next_pad(pad);

    println!("{}-- system name: system", pad);
    let name = string_item(pad2, "system").unwrap();

    println!("{}-- atoms: {}", pad, LIST_MANAGER_USAGE);
    let atoms: Vec<String> = list_manager(pad2, string_item, "atom");

    let system = serde_json::to_string_pretty(&atoms).unwrap();

    create_file(&prefix___sys!(tmpl___sys!(name)), system).unwrap();
}

fn atom(pad: &str) {
    let pad2: &str = &next_pad(pad);

    println!("{}-- atom name: atom", pad);
    let name: String = string_item(pad2, "atom").unwrap();

    println!("{}-- system name: system", pad);
    let system = string_item(pad2, "system").unwrap();

    println!("{}-- redis configuration:", pad);
    let redis_config = redis(pad2, &name);

    let device = device(pad);

    let network_id = Uuid::new_v4();

    println!("{}-- apps: {}", pad, LIST_MANAGER_USAGE);
    let apps: Vec<String> = list_manager(pad2, string_item, "");


    let public_device = device.clone().to_public();

    let atom_data = serde_json::to_string_pretty(&Manifest {
                                                      atom: AtomManifest {
                                                          name: name.clone(),
                                                          network_id: network_id,
                                                      },
                                                      system: system,
                                                      device: device,
                                                      particles: apps,
                                                  })
            .unwrap();

    let public_device = serde_json::to_string_pretty(&public_device).unwrap();

    let redis_config = serde_json::to_string_pretty(&redis_config).unwrap();

    create_dir(&prefix___sys!(tmpl___atom!(name))).is_ok();
    create_file(&prefix___sys_atm!(name, atm___manifest!()), atom_data).unwrap();
    create_file(&prefix___sys_atm!(name, atm___device!()), public_device).unwrap();
    create_file(&prefix___sys_atm!(name, atm___redis!()), redis_config).unwrap();

}

fn redis(pad: &str, name: &str) -> RedisManifest {
    let pad2: &str = &next_pad(pad);

    println!("{}-- config path: ./redis.conf", pad);
    let config = string_item(pad2, "./redis.conf").unwrap();

    println!("{}-- socket path: /tmp/{}/redis.sock", pad, name);
    let socket = string_item(pad2, format!("/tmp/{}/redis.sock", name)).unwrap();

    println!("{}-- redis db #: 0", pad);
    let db: u32 = string_item(pad2, "0").unwrap().parse().unwrap();

    RedisManifest {
        config: config,
        socket: socket,
        db: db,
    }
}

fn app(pad: &str) {
    let pad2: &str = &next_pad(pad);

    println!("{}-- name: app", pad);
    let name = string_item(pad2, "app").unwrap();

    let id = Uuid::new_v4();

    println!("{}-- send actions {}", pad, LIST_MANAGER_USAGE);
    let send: Vec<String> = list_manager(pad2, string_item, "");
    println!("{}-- receive actions {}", pad, LIST_MANAGER_USAGE);
    let receive: Vec<String> = list_manager(pad2, string_item, "");
    println!("{}-- broadcast actions {}", pad, LIST_MANAGER_USAGE);
    let broadcast: Vec<String> = list_manager(pad2, string_item, "");

    let cargo_toml = format!("[package]\r\n\
         name = \"molecule_app__{}\"\r\n\
         version = \"0.1.0\"\r\n\
         authors = []\r\n\
         \r\n\
         [[bin]]\r\n\
         name = \"app\"\r\n\
         path = \"src/app.rs\"\r\n\
         \r\n\
         [dependencies]\r\n\
         futures = \"0.1\"\r\n\
         tokio-core = \"0.1\"\r\n\
         \r\n\
         [dependencies.uuid]\r\n\
         features = [\"v4\", \"serde\"]\r\n\
         version = \"0.4\"\r\n\
         \r\n\
         [dependencies.molecule-common]\r\n\
         #path = \"../../../../molecule-common\"\r\n\
         git = \"https://gitlab.com/may1739-molecule/molecule-common.git\"\r\n",
                             name);

    let manifest = serde_json::to_string_pretty(&ParticleManifest {
                                                     id: id,
                                                     name: name.clone(),
                                                     permissions: PermissionsManifest {
                                                         send: send,
                                                         receive: receive,
                                                         broadcast: broadcast,
                                                     },
                                                 })
            .unwrap();

    create_dir(&prefix___dev!(tmpl___dev_app!(name))).unwrap();
    create_dir(&prefix___dev_app!(name, dev_app___release!())).unwrap();
    create_dir(&prefix___dev_app!(name, dev_app___data!())).unwrap();
    create_dir(&prefix___dev_app!(name, dev_app___src!())).unwrap();

    copy_file(&prefix___resources!(dev_app___ignore!()),
              &prefix___dev_app!(name, dev_app___ignore!()))
            .unwrap();
    copy_file(&prefix___resources!(dev_app___compile!()),
              &prefix___dev_app!(name, dev_app___compile!()))
            .unwrap();
    copy_file(&prefix___resources!(dev_app___setup!()),
              &prefix___dev_app!(name, dev_app___setup!()))
            .unwrap();
    copy_file(&prefix___resources!(app___rs!()),
              &prefix___dev_app!(name, dev_app___rs!()))
            .unwrap();

    create_file(&prefix___dev_app!(name, dev_app___toml!()), cargo_toml).unwrap();
    create_file(&prefix___dev_app!(name, dev_app___manifest!()), manifest).unwrap();
}

/*
fn routing_item<F>(pad: &str, f: F) -> Result<RoutingType, ()>
    where F: Fn(&str, &str) -> Result<Uuid, ()>
{
    let pad2: &str = &next_pad(pad);

    loop {
        let cmd: &str = &string_item(pad2, "one").unwrap();
        match cmd {
            "any" => {
                return Ok(RoutingType::Any);
            }
            "all" => {
                return Ok(RoutingType::All);
            }
            "one" => {
                loop {
                    match f(pad2, "") {
                        Ok(d) => return Ok(RoutingType::One(d)),
                        _ => {
                            println!("Not Found: try again?");
                            let cmd: &str = &string_item(pad2, "yes").unwrap();
                            match cmd {
                                "yes" => continue,
                                _ => return Err(()),
                            }
                        }
                    }
                }
            }
            "some" => {
                return Ok(RoutingType::Some(list_manager(pad2, f, "")));
            }
            _ => continue,
        }
    }


}

fn device_item(pad: &str, _: &str) -> Result<Uuid, ()> {
    let name = read_line(pad);
    if name.is_empty() {
        return Err(());
    }
    let dev_path = prefix___sys_atm!(name, atm___device!());
    if fs::metadata(&dev_path).is_ok() {
        Ok(read_manifest::<DeviceManifest>(dev_path).id)
    } else {
        Err(())
    }

}

fn app_item(pad: &str, _: &str) -> Result<Uuid, ()> {
    let name = read_line(pad);
    if name.is_empty() {
        return Err(());
    }
    let dev_path = prefix___dev_app!(name, dev_app___manifest!());
    let atom_path = prefix___sys_atm!(name, atm___manifest!());
    let app_path = prefix___sys_app!(name, app___manifest!());
    if fs::metadata(&atom_path).is_ok() {
        Ok(read_manifest::<Manifest>(atom_path).device.id)
    } else if fs::metadata(&dev_path).is_ok() {
        Ok(read_manifest::<ParticleManifest>(dev_path).id)
    } else if fs::metadata(&app_path).is_ok() {
        Ok(read_manifest::<ParticleManifest>(app_path).id)
    } else {
        Err(())
    }
}
*/

fn device(pad: &str) -> DeviceManifest {
    let pad2: &str = &next_pad(pad);

    let id = Uuid::new_v4();
    println!("{}-- network address: 127.0.0.1:8080", pad);
    let addr: SocketAddr = string_item(&pad2, "127.0.0.1:8080")
        .unwrap()
        .parse()
        .unwrap();

    let private_device = DeviceManifest::new(id, addr).unwrap();

    private_device
}

static LIST_MANAGER_USAGE: &'static str = "(list, add, finish)";

fn list_manager<F, T, D>(pad: &str, f: F, d: D) -> Vec<T>
    where F: Fn(&str, D) -> Result<T, ()>,
          T: fmt::Debug,
          D: fmt::Debug + Clone
{
    let pad2: &str = &next_pad(pad);
    let mut list: Vec<T> = Vec::new();
    loop {
        let cmd: &str = &read_line(pad);
        match cmd {
            "list" => println!("{}{:?}", pad2, list),
            "add" => {
                let res: Result<T, ()> = f(pad2, d.clone());
                if let Ok(item) = res {
                    list.push(item);
                }
            }
            "finish" => break,
            _ => {
                println!("{}! invalid command", pad2);
                continue;
            }
        }
    }
    list
}

/*fn add_particle() -> String {
    println!("      -- particle name: particle");
    let mut particle = read_line("         ");
    if particle.is_empty() {
        particle = "particle".into();
    }

    particle
}*/

//12
fn string_item<D>(pad: &str, default: D) -> Result<String, ()>
    where D: Into<String>
{
    let mut item = read_line(pad);
    if item.is_empty() {
        let default: String = default.into();
        if default.is_empty() {
            println!("{}! empty string is invalid", pad);
            return Err(());
        }
        item = default;
    }
    Ok(item)
}

fn next_pad(pad: &str) -> String {
    format!("{}   ", pad)
}

fn usage(pad: &str) {
    println!("{}Display Usage", pad);
}
